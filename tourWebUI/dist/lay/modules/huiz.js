﻿
/* Author:Javon Huang 2018/05/18 */
layui.define(['jquery', 'table', 'laydate', 'upload'], function (exports) {
    var form = layui.form
        , table = layui.table
        , laydate = layui.laydate
        , upload = layui.upload
        , $ = layui.jquery;

    var obj = {
        OrderTypeToMvt: function (OrderType) {
            var Result = "";
            switch (OrderType) {
                case "SOI":
                    Result = "普通采购入库";
                    break;
                case "OTI":
                    Result = "外协采购单入库";
                    break;
                case "NDI":
                    Result = "无采购单入库";
                    break;
                case "CSI":
                    Result = "免费入库";
                    break;
                case "CDI":
                    Result = "成本中心退料";
                    break;
                case "ODI":
                    Result = "外协入库(退料)";
                    break;
                case "CUI":
                    Result = "客供料入库";
                    break;
                case "ATI":
                    Result = "调拨入库";
                    break;
                case "URI":
                    Result = "上架/从退货仓转移成品仓或修磨品";
                    break;
                case "OAO":
                    Result = "外协采购单领料";
                    break;
                case "NDO":
                    Result = "无采购单外协领料";
                    break;
                case "DAO":
                    Result = "研发领料";
                    break;
                case "ATO":
                    Result = "调拨出库";
                    break;
                case "DRO":
                    Result = "下架/从成品仓或修磨品仓转移到退货仓";
                    break;
                case "DNO":
                    Result = "交货单出库";
                    break;
                case "SRO":
                    Result = "销售退货";
                    break;
                case "RTV":
                    Result = "供应商退货";
                    break;
                default:
                    Result = "";
                    break;
            }
            return Result;
        },
        OrderTypeToMvtCode: function (OrderType) {
            var Result = "";
            switch (OrderType) {
                case "SOI":
                    Result = "101";
                    break;
                case "OTI":
                    Result = "101";
                    break;
                case "NDI":
                    Result = "501";
                    break;
                case "CSI":
                    Result = "511";
                    break;
                case "CDI":
                    Result = "202";
                    break;
                case "ODI":
                    Result = "542";
                    break;
                case "CUI":
                    Result = "511";
                    break;
                case "ATI":
                    Result = "311";
                    break;
                case "URI":
                    Result = "311";
                    break;
                case "OAO":
                    Result = "541";
                    break;
                case "NDO":
                    Result = "541";
                    break;
                case "DAO":
                    Result = "201";
                    break;
                case "ATO":
                    Result = "311";
                    break;
                case "DRO":
                    Result = "311";
                    break;
                case "DNO":
                    Result = "601";
                    break;
                case "RTV":
                    Result = "601";
                    break;
                case "SRO":
                    Result = "101";
                    break;
                default:
                    Result = "";
                    break;
            }
            return Result;
        },
        ActionToDescr: function (Action) {
            var Result = "";
            switch (Action) {
                case "US":
                    Result = "上架";
                    break;
                case "DS":
                    Result = "下架";
                    break;
                case "IN":
                    Result = "入库";
                    break;
                case "OUT":
                    Result = "出库";
                    break;
                default:
                    Result = "";
                    break;
            }
            return Result;
        }
    }
    //输出接口
    exports('huiz', obj);

});