﻿
/*********分页控件组件********/
Vue.component('jx-page', {
    props: ['value', 'config'],
    template: '<div v-bind:id="config.filter"><input type="text" class="layui-hide"  v-bind:value="value"/></div>',
    data: function () {
        return {
            isclick: false,
            laypage: null,
            common: null,
        }
    },
    created: function () {
        var that = this;
        layui.use(['form', 'common', 'laypage'], function () {
            that.laypage = layui.laypage;
            that.common = layui.common;
            that.$emit('input', that.value);
            that.initvalue();
        });
    },
    updated: function () {
        var that = this;
        if (!that.isclick) {
            that.initvalue();
        }
        that.isclick = false;
    },
    methods: {
        initvalue: function () {
            var that = this;
            switch (that.config.type) {
                case 1:
                    that.laypage.render({
                        elem: that.config.filter
                        , count: that.value.count //数据总数
                        , groups: that.config.groups
                        , jump: function (obj, first) {
                            that.outputmessage(obj);
                            if (!first) {
                                that.isclick = true;
                                that.value.count = obj.count;
                                that.value.page = obj.curr;
                                that.value.limit = obj.limit;
                                that.$emit("query", that.value)
                                that.$emit('input', that.value);
                            }
                        }
                    });
                    break;
                case 2:
                    that.laypage.render({
                        elem: that.config.filter
                        , count: that.value.count
                        , layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip']
                        , jump: function (obj, first) {
                            that.outputmessage(obj);
                            if (!first) {
                                that.isclick = true;
                                that.value.count = obj.count;
                                that.value.page = obj.curr;
                                that.value.limit = obj.limit;
                                that.$emit("query", that.value);
                                that.$emit('input', that.value);
                            }
                        }
                    });
                    break;
            }
        },
        outputmessage: function (d) {
            var that = this;
            //if (that.common.getconfig.debug) {
            //    console.log({
            //        "type": "page",
            //        "config": that.config,
            //        "filter": that.config.filter,
            //        "selected": d,
            //    });
            //}
        }
    }
})
/*********多图片上传控件组件********/
Vue.component('jx-upload-mult-img', {
    props: ['filter', 'value'],
    template: '<input type="text" v-bind:value="value" class="layui-hide">',
    data: function () {
        return {
            uploadListIns: null
        }
    },
    created: function () {
        var that = this;
        layui.use(['form', 'upload', 'common'], function () {
            var form = layui.form
                , common = layui.common
                , upload = layui.upload;
            that.uploadListIns = upload.render({
                elem: '#' + that.filter
                , url: '#'
                , accept: 'file'
                , exts: 'png|jpg|gif|jpeg'
                , multiple: true
                , auto: false
                , choose: function (obj) {
                    obj.preview(function (index, file, result) {
                        that.$emit('input', { file: file, base64: result });
                        that.uploadListIns.config.elem.next()[0].value = '';
                    });
                }
            });
        });
    }
})
/******数字输入框*****/
Vue.component('jx-input-num-0', {
    props: ['value'],
    data: function () {
        return {
            styleserror: {
                border: '',
            },
            stylessucce: {
                border: '',
            },
            cla: null,
            timeout: null
        }
    },
    template: '<input type="text" v-bind:style="cla" v-bind:value="value" v-on:input="update($event)">',
    methods: {
        update: function (val) {
            var that = this;
            clearTimeout(that.timeout);
            that.timeout = setTimeout(function () {
                that.setvalue(val);
            }, 1000);
        },
        setMsg: function (d) {
            var that = this;
            if (!d) {
                that.cla = that.styleserror;
                layer.msg("请输入大于0的数字", { icon: 5, time: 500 });
            } else {
                that.cla = that.stylessucce;
            }
        },
        setvalue: function (val) {
            var that = this;
            if (val.target.value === "") {
                val.target.value = 0;
                that.$emit('input', "0");
            } else {
                var result = false;
                if (val.target.value.substring(0, 1) === "0") {
                    result = false;
                }
                else if (isNaN(val.target.value)) {
                    result = false;
                } else {
                    if (parseFloat(val.target.value) > 0) {
                        result = true;
                    } else {
                        result = false;
                    }
                }

                if (!result) {
                    val.target.value = 0;
                    that.$emit('input', "0");
                    that.setMsg(false);
                } else {
                    that.$emit('input', val.target.value);
                    that.setMsg(true);
                }
            }
        }
    }
});
