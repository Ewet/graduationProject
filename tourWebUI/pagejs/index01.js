/**
 * Created by ka on 2019/2/26.
 */
var urlhome = "";
var table, $, laydate, layer, common, element, rate, form, formSelects;
var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
    app.initpage();
    document.getElementById("framea").src = "../User/US001.html";
    urlhome = "../User/US001.html";
});

var app = new Vue({
    el: '#app',
    data: {
        menuobj: {
            homeurl: "",
            menu: []
        },
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
            index_4: 4,
            index_5: 5,
        },
        roleName:"",
        userinfo:"",

    },
    created: function () {
        var that = this;
    },
    updated: function () {
        form.render();
        element.init();
    },
    methods: {
        initpage: function () {
            var that = this;
            that._data.userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
            console.log(that._data.userinfo)
            if(that._data.userinfo.role == 2){
                that._data.roleName = "系统管理员";
            }else if(that._data.userinfo.role == 1){
                that._data.roleName = "工作人员";
            }
            common.ajax_post("/travel/auth/getMenu",null, true,
                function (data) {
                    if (data.code === 0) {
                        console.log(data);
                        if (data.msg == "SUCC") {
                            that._data.menuobj.homeurl = urlhome;
                            that._data.menuobj.menu = data.data;
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    }
                });
        },
        showposition: function (first, second) {
            var that = this;
            that._data.showtittle = first.Descr + " > " + second.Descr;
        },
    }

});