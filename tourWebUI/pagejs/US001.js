/**
 * Created by ka on 2019/3/5.
 */
var table, $, laydate, layer, common, element, rate, form, formSelects;
var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
    app.initpage();
});

var app = new Vue({
    el: '#app',
    data: {
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
        },
        tableins: {
            view_1_table_1_ins: null
        },
        qdata:{
            userId:""
        },
        editObj:{
            userName:"",
            tele:"",
            password:"",
            email:"",
            userId:"",
            role:"",
            sex:""
        },
        userData:"",
        userinfo:"",
        indexSe:"",

    },
    created: function () {
        var that = this;
    },
    updated: function () {
        form.render();
        element.init();
                },
        methods: {
            initpage: function(){
            var that = this;
            that._data.userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
            that._data.qdata.userId = that._data.userinfo.userId;
            common.ajax_post("/travel/admin/getById",JSON.stringify(that._data.qdata), true,
                function (data) {
                    if (data.code === 0) {
                        if (data.msg == "SUCC") {
                            that._data.userData = data.data;
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    }});
        },
        showUpdatePage: function () {
            var that = this;
            that._data.editObj.userId = that._data.userData.userId;
            that._data.editObj.userName = that._data.userData.userName;
            that._data.editObj.tele =that._data.userData.tele;
            that._data.editObj.password = that._data.userData.password;
            that._data.editObj.email = that._data.userData.email;
            that._data.editObj.sex= that._data.userData.sex;
            if(that._data.userData.role == 1){
                that._data.editObj.role =1;
            }else if(that._data.userData.role == 2){
                that._data.editObj.role =2;
            }
            that._data.layerindex.index_1 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                skin: 'background-full',
                shade: 0.1,
                title:  '<span class="tit"><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;修改</span>',
                content: $("#view_1"),
                area: ["600px", "340px"]
            });
        },
            saveEdit: function () {
                var that = this;
                //监听提交
                that._data.editObj.sex = $("#selectSex").val();
                form.on('submit(formedit)', function (data) {
                    common.ajax_post('/travel/admin/updateUser',JSON.stringify(that._data.editObj) , true,
                        function (data) {
                            if (data.code == 0) {
                                layer.msg("个人信息修改成功！", { icon: 1 });
                                layer.close(that._data.layerindex.index_1);
                                that.initpage();
                            } else {
                                layer.msg(data.message, { icon: 5 });
                            }
                        });
                    return false;
                });
            },
            cancel: function () {
                var that = this;
                layer.close(that._data.layerindex.index_1);
            },
    }

});