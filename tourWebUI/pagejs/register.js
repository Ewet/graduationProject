/**
 * Created by ka on 2019/1/2.
 */
var  $, layer, common, element, form, userinfo;
layui.use(['element', 'layer', 'common', 'form'], function () {
    table = layui.table
        , $ = layui.jquery
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , form = layui.form;
    app.createCode();
});
var app = new Vue({
    el: '#app',
    data: {
        registerData: {
            userName: '',
            tele: '',
            email:'',
            password:'',
            sex:'',
            role:0,
        },
        passSure:"",
        userList:{
            tele:"",
        },
        codeSure:''
    },
    created: function () {
    },
    methods: {
        register: function () {
            var that = this;
            var inputCode=document.getElementById("inputCode").value;
            var val=$('input:radio[name="sex"]:checked').val();
            that._data.registerData.sex = val;
            if(that._data.registerData.tele ==''){
                layer.msg('手机号码不能为空！', { icon: 5 });
                return false;
            }
            if(that._data.registerData.userName ==''){
                layer.msg('请输入用户名', { icon: 5 });
                return false;
            }
            if(that._data.registerData.password ==''){
                layer.msg('请输入密码', { icon: 5 });
                return false;
            }
            if(that._data.passSure==''){
                layer.msg('请输入确认密码', { icon: 5 });
                return false;
            }
            if(that._data.registerData.email ==''){
                layer.msg('邮箱不能为空', { icon: 5 });
                return false;
            }
            if(!(/^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/.test(that._data.registerData.email))) {
                layer.msg('请填写正确的邮箱！', { icon: 5 });
                return false;
            }
            if(inputCode.length <= 0)
            {
                layer.msg("请输入验证码",{icon:5});
                return false;
            }
            else if(inputCode.toUpperCase() != that._data.codeSure.toUpperCase())
            {
                layer.msg("验证码输入有误!",{icon:5 });
                that.createCode();
                return false;
            }
            common.ajax_post("/travel/auth/regist",JSON.stringify(that._data.registerData), true,
                function (data) {
                    var a = data;
                    if (data.msg == "SUCC") {
                        layer.msg(that._data.registerData.tele+'注册成功', { icon: 5 });
                    } else {
                        layer.msg(data.message, { icon: 5 });
                    }
                }
            );
        },
        createCode:function(){
            var that = this;
            var code = "";
            var codeLength = 4;  //验证码的长度
            var checkCode = document.getElementById("checkCode");
            var codeChars = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'); //所有候选组成验证码的字符，当然也可以用中文的
            for(var i = 0; i < codeLength; i++)
            {
                var charNum = Math.floor(Math.random() * 52);
                code += codeChars[charNum];
            }
            if(checkCode)
            {
                checkCode.className = "code";
                checkCode.innerHTML = code;
            }
            that._data.codeSure =code;
        },
        validateCode:function(){
            var inputCode=document.getElementById("inputCode").value;
            if(inputCode.length <= 0)
            {
                alert("请输入验证码！");
            }
            else if(inputCode.toUpperCase() != code.toUpperCase())
            {
                alert("验证码输入有误！");
                createCode();
            }
            else
            {
                alert("验证码正确！");
            }
        },
        //检查用户是否存在
        userExist:function(){
            var that = this;
            that._data.userList.tele = that._data.registerData.tele;
            if(!(/^1(3|4|5|7|8)\d{9}$/.test(that._data.registerData.tele))){
                layer.msg('请填写正确的手机号码！', { icon: 5 });
                return false;
            }else{
                common.ajax_post("/travel/auth/checkUserExist",JSON.stringify(that._data.userList), true,
                    function (data) {
                        if (data.msg == "SUCC") {
                            if(data.data==true){
                                that._data.registerData.userName = '';
                                layer.msg('该用户已存在！', { icon: 5 });
                            }
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    }
                );
            }
        },
        testPassword:function(){
            var that =this;
            if(that._data.registerData.password!=that._data.passSure){
                layer.msg('两次密码不一致,请重新输入！', { icon: 5 });
                that._data.passSure = '';
            }
        },

    },
});