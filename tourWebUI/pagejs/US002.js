/**
 * Created by ka on 2019/3/15.
 */
var table, $, laydate, layer, common, element, rate, form, formSelects;
var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
    app.initpage();
});

var app = new Vue({
    el: '#app',
    data: {
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
            index_4: 4,
        },
        tableins: {
            view_1_table_1_ins: null
        },
        qdata:{
            role:"",
            userId:""
        },
        editObj:{
            userName:"",
            tele:"",
            password:"",
            email:"",
            userId:"",
            role:"",
            sex:""
        },
        newObj:{
            userName:"",
            tele:"",
            password:"123456",
            email:"",
            role:1,
            sex:1
        },
        delData:{
            userId:""
        },
        userinfo:"",

    },
    created: function () {
        var that = this;
    },
    updated: function () {
        form.render();
        element.init();
    },
    methods: {
        initpage: function () {
            var that = this;
            that._data.userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
            that._data.qdata.role = 1;
            that._data.qdata.userId = that._data.userinfo.userId;
            that._data.tableins.view_1_table_1_ins = table.render({
                elem: '#view_1_table_1'
                , url: common.ajax_g_url() + '/travel/admin/getByRole'
                , method: 'post'
                ,contentType:"application/json"
                ,where: that.qdata
                , page: {
                    layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
                    , limit: 10
                    , groups: 5 //只显示 1 个连续页码
                }
                , cellMinWidth: 80
                , cols: [[
                    { type: 'numbers' }
                    , { field: 'userName', title: '用户名', minWidth:80, align: 'center' }
                    //, { field: 'tele', title: '手机号码', minWidth: 110, align: 'center' }
                    , { field: 'tele', title: '登录账号', minWidth: 110, align: 'center' }
                    , { field: 'password', title: '密码', minWidth: 80, align: 'center' }
                    , { field: 'email', title: '邮箱', minWidth: 150, align: 'center' }
                    , {
                        field: 'role', title: '性别', minWidth: 80, align: 'center', templet: function (d) {
                            switch (d.sex) {
                                case 0:
                                    return '女';
                                    break;
                                case 1:
                                    return '男';
                            }
                        }}
                    , {
                        field: 'role', title: '角色', minWidth: 90, align: 'center', templet: function (d) {
                            switch (d.role) {
                                case 0:
                                    return '游客';
                                    break;
                                case 1:
                                    return '旅社员工';
                                case 2:
                                    return '系统管理员';
                                    break;
                            }
                        }}
                    , {
                        field: '', title: '操作',align:'center',fixed: 'right', width: 150, templet: function (d) {
                            return  '<a class="app-btn" lay-event="edit" > <i class="iconfont icon-bianji"></i>修改</a>'+'<a class="app-btn" lay-event="del" > <i class="iconfont icon-shanchu"></i>删除</a>'
                        }
                    }
                ]]
                , done: function (res, curr, count) {
                    table_data = res.data;
                    if(res.code!=0){
                        $('.layui-none').html('暂无相关数据');
                    }
                }
            });
            table.on('tool(view_1_table_1)', function (obj) {
                var data = obj.data
                if (obj.event === 'edit') {
                    that.showUpdatePage(data);
                }
                if(obj.event === 'del'){
                    that.del(data);
                }
            })
        },
        showUpdatePage: function (d) {
            var that = this;
            that._data.editObj.userId = d.userId;
            that._data.editObj.userName = d.userName;
            that._data.editObj.tele =d.tele;
            that._data.editObj.password = d.password;
            that._data.editObj.email =d.email;
            that._data.editObj.role = d.role;
            that._data.editObj.sex= d.sex;
            that._data.layerindex.index_1 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                skin: 'background-full',
                shade: 0.1,
                title:  '<span class="tit"><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;修改员工信息</span>',
                content: $("#view_1"),
                area: ["600px", "340px"]
            });
        },
        saveEdit: function () {
            var that = this;
            //监听提交
            that._data.editObj.sex = $("#selectSex").val();
            form.on('submit(formedit)', function (data) {
                common.ajax_post('/travel/admin/updateUser',JSON.stringify(that._data.editObj) , true,
                    function (data) {
                        if (data.code == 0) {
                            layer.msg("员工信息修改成功！", { icon: 1 });
                            layer.close(that._data.layerindex.index_1);
                            that.initpage();
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    });
                return false;
            });
        },
        del: function (d) {
            var that = this;
            that._data.delData.userId = d.userId;
            var confirm = layer.confirm('确认删除该员工吗！', {
                btn: ['确定', '取消'] //按钮
                , title: '提示',
            }, function () {
                common.ajax_post('/travel/admin/deleUser',JSON.stringify(that._data.delData), true,
                    function (data) {
                        if (data.code == 0) {
                            layer.msg("删除成功", { icon: 1 });
                            //layer.close(that._data.layerindex.index_2);
                            that.initpage();
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    });
            }, function () {
                layer.close();
            });
        },
        showAddPage: function () {
            var that = this;
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                skin: 'background-full',
                shade: 0.1,
                title:  '<span class="tit"><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;添加员工</span>',
                content: $("#view_2"),
                area: ["600px", "340px"]
            });
        },
        saveNew: function () {
            var that = this;
            //监听提交
            form.on('submit(formnew)', function (data) {
                that._data.newObj.sex = $("#newSex").val();
                common.ajax_post('/travel/admin/createUser',JSON.stringify(that._data.newObj) , true,
                    function (data) {
                        if (data.code == 0) {
                            layer.msg("添加成功", { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                            that.initpage();
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    });
                return false;
            });
        },
        cancel: function () {
            var that = this;
            layer.close(that._data.layerindex.index_1);
            layer.close(that._data.layerindex.index_2);
        },
        exportExcel:function () {
            var that = this;
            that.layerindex.index_4 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            common.ajax_post("/travel/admin/getUserList",JSON.stringify(that._data.qdata), true,
                function (data) {
                    if (data.code === 0) {
                        layer.close(that.layerindex.index_4);
                        common.execl_export_render(data.data,"用户管理", "xlsx",
                            [["userName", "用户名"],["email","邮箱"],["tele","手机号码"],["sex","性别"]],
                            ["userId","password","createBy","createTime","updateBy","updateTime","role"]);
                    } else {
                        layer.close(that.layerindex.index_4);
                        layer.msg(data.msg, { icon: 5 });
                    }
                });
        },
    }

});