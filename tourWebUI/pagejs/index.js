/**
 * Created by ka on 2019/1/16.
 */
var  $, layer, common, element, form, userinfo;
layui.use(['element', 'layer', 'common', 'form'], function () {
    table = layui.table
        , $ = layui.jquery
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , form = layui.form;
    app.initPage();
});
var app = new Vue({
    el: '#app',
    data: {
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
        },
        view: {
            view_1: false,
        },
        userinfo:"",
        areaId2:{
            areaId:"",
        },
        newsTopList:"",
    },
    created: function () {

    },
    methods: {
        initPage: function () {
            var that = this;
            that._data.userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
            that._data.areaId2.areaId = "";
            common.ajax_post('/travel/travel/getLocationByIp',JSON.stringify(), true,
                function (data) {
                    if (data.code == 0){
                    }else {
                        //layer.msg(data.message, { icon: 5 });
                }
            });
            common.ajax_post('/travel/travel/getNewsTopTen',JSON.stringify(that._data.areaId2), true,
                function (data) {
                    if (data.code === 0) {
                        that._data.newsTopList = data.data;
                    } else {
                        layer.msg(data.message, { icon: 5 });
                    }
                });
        },
        showDetailPage: function (d) {
            var that = this;
            console.log(that._data.userinfo);
            if(that._data.userinfo == ""||that._data.userinfo==null){
                layer.msg("您还没登录，登录后可立即预定！", { icon: 5 });
            }else{
                that._data.layerindex.index_2 = layer.open({
                    type: 2,
                    offset: 'auto',
                    shadeClose: true,
                    skin: 'background-full',
                    shade: 0.1,
                    title:  '<span class="tit"></span>',
                    content: "../orderDetail/orderDetail.html",
                    area: ["100%", "100%"],
                    success: function (index, layero) {
                        that._data.iframeWin = window[index.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        that._data.iframeWin.app.orderData = d;
                    },
                });
                $('.title').hide();
                layer.full(that._data.layerindex.index_2);
            }
        },
        orderDetailPage: function () {
            var that = this;
                that._data.layerindex.index_3 = layer.open({
                    type: 2,
                    offset: 'auto',
                    shadeClose: true,
                    skin: 'background-full',
                    shade: 0.1,
                    title:  '<span class="tit"></span>',
                    content: "myOrder.html",
                    area: ["100%", "100%"],
                    //success: function (index, layero) {
                    //    that._data.iframeWin = window[index.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                    //    that._data.iframeWin.app.orderData = d;
                    //},
                });
                $('.title').hide();
                layer.full(that._data.layerindex.index_3);
        },
    }

});