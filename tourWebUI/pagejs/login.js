/**
 * Created by ka on 2019/2/26.
 */
var  $, layer, common, element, form,userinfo;
layui.use(['element', 'layer', 'common', 'form'], function () {
    table = layui.table
        , $ = layui.jquery
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , form = layui.form;
});
var app = new Vue({
    el: '#app',
    data: {
        layerindex: {
            index_1: 1,
        },
        view: {
            view_1: false,
        },
        logindata: {
            tele: '',
            password: '',
        },
    },
    created: function () {

    },
    methods: {
        login: function () {
            var that = this;
            common.ajax_post("/travel/auth/login", JSON.stringify(that._data.logindata), true,
                function (data) {
                    if (data.msg == "SUCC"){
                        window.sessionStorage.setItem("userInfo", JSON.stringify(data.data));
                        if(data.data.role == 2||data.data.role == 1){
                            window.location.href = "../Admins/index01.html";
                        //}else if(data.data.role == 1){
                        }else{
                            layer.msg("账号或密码错误", { icon: 5 });
                        }
                    } else {
                        layer.msg("账号或密码错误", { icon: 5 });
                    }
                }
            );
        },
        //resetPasswork: function () {
        //    var that = this;
        //    that._data.layerindex.index_1 = layer.open({
        //        type: 1,
        //        offset: 'auto',
        //        shade: 0,
        //        title: ["&nbsp;", "height:0px;"],
        //        content: $("#view_1"),
        //        area: ["430px", "340px"],
        //        closeBtn: 1,
        //    });
        //},
    }
});