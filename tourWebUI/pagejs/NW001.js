var $, form, layer, common, lodash, carousel, element;
layui.use(['element','jquery', 'form', 'layer', 'common', 'lodash', 'carousel'], function () {
    $ = layui.jquery
        , layer = layui.layer
        , lodash = layui.lodash
        , carousel = layui.carousel
        , form = layui.form
        , common = layui.common
        , element = layui.element;
    app.initpage();
});
Vue.use(window.VueAwesomeSwiper);
var app = new Vue({
    el: '#app',
    data: {
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
            index_4: 4,
        },
        tableins: {
            view_1_table_1_ins: null
        },
        qdata:{
            currentPage:1,
            pageSize:10,
        },
        pageconfig: {
            filter: "page_id",
            type: 2,
            count: 0,
            groups: 10,
        },
        swiperOptionTop: {
            spaceBetween: 10,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            }
        },
        swiperOptionThumbs: {
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true
        },
        pageInfo: { count: 0, page: 1, limit: 10 },
        productList: [{
            img:[],
        }],//项目列表
        deleteData:{
            newsId:"",
        },
        chosenProductInfo:"",


    },
    created: function () {
        var that = this;
    },
    updated: function () {
        form.render();
        element.init();
    },
    methods: {
        initpage: function () {
            var that = this;
            common.ajax_post('/travel/travel/getNews', JSON.stringify(that._data.qdata),true,
                function (data) {
                    if (data.code == 0) {
                        that._data.pageInfo = { count: data.count, page: data.currentPage, limit: data.pageSize };
                        that._data.productList = data.data;
                    } else {
                        layer.msg(data.msg, { icon: 5 });
                    }
                })
        },
        //分页触发
        queryPage: function (d) {
            var that = this;
            that._data.qdata.currentPage = d.page;
            that._data.qdata.pageSize = d.limit;
            that.initpage();
        },
        //获取产品列表
        query: function () {
            var that = this;
            that._data.qdata.currentPage = 1;
            that.initpage();
        },
        //列表图片浏览
        showPhoto: function (d) {
            console.log(d)
            var json = {
                title: "",
                id: "",
                start: "",
                data: []
            }
            lodash.forEach(d, function (value, key) {
                json.data.push({
                    alt: "",
                    pid: "",
                    src: value.img,
                    thumb: ""
                });
            });
            layer.photos({
                photos: json
                , anim: 5
            });
        },
        //详情弹出层
        showDetailPage: function (d) {
            var that = this;
            that._data.chosenProductInfo = d;
            if (that._data.chosenProductInfo.img === null || that._data.chosenProductInfo.img.length === 0) {
                that._data.chosenProductInfo.img = [];
                that._data.chosenProductInfo.img.push({ img: "../../img/nopic.png" });
            }
            that._data.layerindex.index_1 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                skin: 'background-full',
                shade: 0.1,
                title:  '<span class="tit"><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;查看详情</span>',
                content: $("#view_1"),
                area: ["580px", "320px"]
            });
        },
        //删除项目
        deletePruduct: function(d){
            var vm = this;
            vm.deleteData.newsId = d.newsId;
            var confirm = layer.confirm('确定删除该项目吗？',{
                    btn:['确定','取消']
                },function(index,layero){
                    common.ajax_post('/Edison/supplier/delProduct',JSON.stringify(that._data.deleteData),
                        function (data) {
                            if(data.code == 0){
                                vm.getProductList();
                                layer.msg('商品删除成功',{icon:1});

                            }else{
                                layer.msg(data.msg,{icon:5});
                            }
                        })
                },function(index){
                    layer.close(confirm);
                }
            )},
        showAddPage: function () {
            var that = this;
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                skin: 'background-full',
                shade: 0.1,
                title:  '<span class="tit"><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;添加新项目</span>',
                content: $("#view_2"),
                area: ["760px", "500px"]
            });
        },
    }
});