/**
 * Created by ka on 2019/1/8.
 */
var urlhome = "";
var table, $, laydate, layer, common, element, rate, form, formSelects;
var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
    //app.initpage();
    //if (userinfo.role.roleId === 2) {
    //    document.getElementById("framea").src = "panel.html";
    //    urlhome = "panel.html";
    //} else if (userinfo.role.roleId === 5) {
    //    document.getElementById("framea").src = "../UserManage/ST001.html";
    //    urlhome = "../UserManage/ST001.html";
    //} else if (userinfo.role.roleId === 3) {
    //    document.getElementById("framea").src = "newPanel.html";
    //    urlhome = "newPanel.html";
    //    //urlhome = "main.html";
    //}
});

var app = new Vue({
    el: '#app',
    data: {
        menuobj: {
            homeurl: "",
            menu: []
        },
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
            index_4: 4,
            index_5: 5,
        },

    },
    created: function () {
        var that = this;
    },
    updated: function () {
        form.render();
        element.init();
    },
    methods: {

    }
});