/**
 * Created by ka on 2019/3/19.
 */
var table, $, laydate, layer, common, element, rate, form, formSelects;
var userinfo = JSON.parse(sessionStorage.getItem("userInfo"));
layui.use(['element', 'rate', 'table', 'layer', 'common', 'form', 'laydate', 'formSelects'], function () {
    table = layui.table
        , $ = layui.jquery
        , laydate = layui.laydate
        , layer = layui.layer
        , common = layui.common
        , element = layui.element
        , rate = layui.rate
        , formSelects = layui.formSelects
        , form = layui.form;
    app.initpage();
});

var app = new Vue({
    el: '#app',
    data: {
        layerindex: {
            index_1: 1,
            index_2: 2,
            index_3: 3,
            index_4: 4,
        },
        tableins: {
            view_1_table_1_ins: null
        },
        qdata:{
        },
        editObj:{
        },
        newObj:{
            newsId:"",
        },
        pageconfig: {
            filter: "page_id",
            type: 2,
            count: 0,
            groups: 10,
        },
        pageInfo: { count: 0, page: 1, limit: 10 },
        orderList:[],
        delData:{
            orderId:"",
        },
        getNewsData:{
            currentPage:1,
            pageSize:100,
        },
    },
    created: function () {
        var that = this;
    },
    updated: function () {
        form.render();
        element.init();
    },
    methods: {
        initpage: function () {
            var that = this;
            common.ajax_post('/travel/order/getOrder', JSON.stringify(that._data.qdata),true,
                function (data) {
                    if (data.code == 0) {
                        that._data.pageInfo = { count: data.count, page: data.currentPage, limit: data.pageSize };
                        that._data.orderList = data.data;
                    } else {
                        layer.msg(data.msg, { icon: 5 });
                    }
                })
        },
        //分页触发
        queryPage: function (d) {
            var that = this;
            that._data.qdata.currentPage = d.page;
            that._data.qdata.pageSize = d.limit;
            that.initpage();
        },
        //获取产品列表
        query: function () {
            var that = this;
            that._data.qdata.currentPage = 1;
            that.initpage();
        },
        del: function (d) {
            var that = this;
            that._data.delData.orderId = d.orderId;
            var confirm = layer.confirm('确认删除该订单吗！', {
                btn: ['确定', '取消'] //按钮
                , title: '提示',
            }, function () {
                common.ajax_post('/travel/order/deleteOrder',JSON.stringify(that._data.delData), true,
                    function (data) {
                        if (data.code == 0) {
                            layer.msg("删除成功", { icon: 1 });
                            that.initpage();
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    });
            }, function () {
                layer.close();
            });
        },
        showAddPage: function () {
            var that = this;
            common.Select(null, { id: "newsId", text: "title" },
                "/travel/travel/getNews", "post", JSON.stringify(that._data.getNewsData), "newOrder");
            that._data.layerindex.index_2 = layer.open({
                type: 1,
                offset: 'auto',
                shadeClose: true,
                skin: 'background-full',
                shade: 0.1,
                title:  '<span class="tit"><i class="iconfont icon-yonghuguanli" style="color:#3298cf"></i>&nbsp;添加新订单</span>',
                content: $("#view_2"),
                area: ["620px", "340px"]
            });
        },
        saveNew: function () {
            var that = this;
            //监听提交
            form.on('submit(formnew)', function (data) {
                common.ajax_post('/travel/order/createOrder',JSON.stringify(that._data.newObj) , true,
                    function (data) {
                        if (data.code == 0) {
                            layer.msg("添加成功", { icon: 1 });
                            layer.close(that._data.layerindex.index_2);
                            that.initpage();
                        } else {
                            layer.msg(data.message, { icon: 5 });
                        }
                    });
                return false;
            });
        },
        cancel: function () {
            var that = this;
            layer.close(that._data.layerindex.index_1);
            layer.close(that._data.layerindex.index_2);
        },
        exportExcel:function () {
            var that = this;
            that.layerindex.index_4 = layer.load(1, {
                shade: [0.1, '#fff'] //0.1透明度的白色背景
            });
            common.ajax_post("/travel/admin/getUserList",JSON.stringify(that._data.qdata), true,
                function (data) {
                    if (data.code === 0) {
                        layer.close(that.layerindex.index_4);
                        common.execl_export_render(data.data,"用户管理", "xlsx",
                            [["userName", "用户名"],["email","邮箱"],["tele","手机号码"],["sex","性别"]],
                            ["userId","password","createBy","createTime","updateBy","updateTime","role"]);
                    } else {
                        layer.close(that.layerindex.index_4);
                        layer.msg(data.msg, { icon: 5 });
                    }
                });
        },
    }

});